/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.swfteaproject;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.swftea.project";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 6;
  public static final String VERSION_NAME = "3.0.6";
}
