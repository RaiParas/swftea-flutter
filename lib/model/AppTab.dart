import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_pusher/pusher.dart';
import 'package:get/get.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:swfteaproject/model/Emoji.dart';
import 'package:swfteaproject/model/EmojiCategory.dart';
import 'package:swfteaproject/model/Message.dart';
import 'package:swfteaproject/model/MessageContainer.dart';
import 'package:swfteaproject/providers/SwfTeaController.dart';
import 'dart:io' as io;
import 'package:swfteaproject/ui/widgets/generic/nokeyboardFocusNode.dart';
import 'package:path_provider/path_provider.dart';

class AppTab {
  final String type;
  final String label;
  final String key;
  bool blinking = false;
  bool isRecording = false;
  bool active;
  bool emojiShown = false;
  bool galleryShown = false;
  PageController emojiTab = new PageController();
  Channel channel;
  Channel notificationchannel;
  List<SwfTeaMessage> messages = [];
  MessageContainer messageContainer;
  TextEditingController textBox = new TextEditingController();
  ScrollController scrollController = new ScrollController();
  FocusNode textFocusNode = new NoKeyboardEditableTextFocusNode();
  List<String> selectedimages = [];
  FlutterAudioRecorder recorder;
  bool submenuOpened = false;

  List<Map<String, dynamic>> images;

  AppTab(
    this.type,
    this.label,
    this.key, {
    this.active = false,
    this.blinking = false,
    this.emojiShown = false,
    this.galleryShown = false,
    this.messageContainer,
  }) {
    loadImage();
  }

  void loadImage() async {
    var result = await PhotoManager.requestPermission();
    if (result) {
      try {
        List<AssetPathEntity> list = await PhotoManager.getAssetPathList();
        AssetPathEntity path = list[0];
        List<AssetEntity> entity = await path.assetList;

        List<Map<String, dynamic>> imagebytes = [];
        Uint8List bytes;
        AssetEntity entry;
        print("Entity");
        print(entity);

        int len = entity.length;
        for (int index = 0; index < len; index++) {
          entry = entity[index];
          File file = await entry.file;
          print(file.path);
          try {
            bytes = await entry.thumbData;
            imagebytes.add({"image": bytes, "path": file.path});
          } catch (e) {}
        }
        images = imagebytes;
      } catch (e) {
        print(e);
      }
    } else {}
  }

  initRecording() async {
    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String customPath = '/flutter_audio_recorder_';
        io.Directory appDocDirectory;
        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // .wav <---> AudioFormat.WAV
        // .mp4 .m4a .aac <---> AudioFormat.AAC
        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        recorder =
            FlutterAudioRecorder(customPath, audioFormat: AudioFormat.WAV);

        await recorder.initialized;
        // after initialization
        var current = await recorder.current(channel: 0);
        print(current);
        // should be "Initialized", if all working fine

      } else {}
    } catch (e) {
      print(e);
    }
  }

  openGalleryBoard() {
    this.emojiShown = false;
    this.galleryShown = true;
  }

  closeGalleryBoard() {
    this.emojiShown = false;
    this.galleryShown = false;
  }

  openEmojiBoard() {
    this.galleryShown = false;
    this.emojiShown = true;
  }

  closeEmojiBoard() {
    this.galleryShown = false;
    this.emojiShown = false;
  }

  setChannel(Channel channel) {
    this.channel = channel;
  }

  setNotificationChannel(Channel channel) {
    this.notificationchannel = channel;
  }

  addMessage(SwfTeaMessage message) {
    this.messages.add(message);
  }
}
