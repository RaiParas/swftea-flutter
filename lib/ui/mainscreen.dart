import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:swfteaproject/constants/constants.dart';
import 'package:swfteaproject/providers/SwfTeaController.dart';
import 'package:swfteaproject/providers/UserProvider.dart';
import 'package:swfteaproject/ui/widgets/customTabView.dart';
import 'package:swfteaproject/ui/widgets/generic/drawer.dart';
import 'package:swfteaproject/ui/widgets/generic/rippleanimation.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light.copyWith(
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Theme.of(context).primaryColor,
        statusBarIconBrightness: Brightness.light,
        statusBarColor: Theme.of(context).primaryColor, // Note RED here
      ),
    );

    Future<bool> _onWillPop() async {
      return (await showDialog(
            context: context,
            builder: (context) => new AlertDialog(
              title: new Text('Are you sure?'),
              content: new Text('Do you want to exit SwfTea'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: new Text('No'),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Yes'),
                ),
              ],
            ),
          )) ??
          false;
    }

    UserProvider userProvider = Provider.of<UserProvider>(context);
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    return WillPopScope(
      onWillPop: () => _onWillPop(),
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          key: _scaffoldKey,
          // appBar: AppBar(
          //   title: Text('SWFTEA'),
          //   actions: <Widget>[
          //     SizedBox(
          //       child: Center(
          //         child: GlobalAudio(),
          //       ),
          //       width: 55.0,
          //       height: 55.0,
          //     ),
          //     IconButton(
          //       icon: Icon(Icons.shopping_basket, size: 20),
          //       onPressed: () {
          //         Get.toNamed(GIFTS_SCREEN);
          //       },
          //     ),
          //     Padding(
          //       padding: const EdgeInsets.only(right: 25.0),
          //       child: IconButton(
          //         icon: Icon(Icons.notifications, size: 20),
          //         onPressed: () {
          //           Get.toNamed(NOTIFICATION_SCREEN);
          //         },
          //       ),
          //     ),
          //   ],
          // ),
          drawer: MainDrawer(skey: _scaffoldKey),
          drawerEnableOpenDragGesture: true,
          body: GetBuilder<Controller>(
            init: Controller(user: userProvider.user),
            builder: (_) => CustomTabView(() {
              _scaffoldKey.currentState.openDrawer();
            }),
          ),
        ),
      ),
    );
  }
}

class GlobalAudio extends StatefulWidget {
  final audioPlayer = AudioPlayer();
  @override
  _GlobalAudioState createState() => _GlobalAudioState();
}

class _GlobalAudioState extends State<GlobalAudio>
    with SingleTickerProviderStateMixin {
  bool isPlaying = false;
  LocalStorage storage = new LocalStorage('swftea_app');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: storage.ready,
      builder: (context, snapshot) {
        if (snapshot.data == true) {
          if (storage.getItem('radio_animation_show') ?? true && !isPlaying) {
            return RipplesAnimation(
              color: Colors.green,
              onPressed: () {
                storage.setItem('radio_animation_show', false);
                widget.audioPlayer.play(
                  "http://stream.zeno.fm/hxm8m339xa0uv",
                );
                setState(() {
                  isPlaying = true;
                });
              },
              child: Icon(
                Icons.wifi_tethering,
                color: Colors.white,
              ),
            );
          } else {
            return isPlaying
                ? RipplesAnimation(
                    color: Colors.red,
                    onPressed: () {
                      widget.audioPlayer.release();
                      setState(() {
                        isPlaying = false;
                      });
                    },
                    child: Icon(
                      Icons.wifi_tethering,
                      color: Colors.white,
                    ),
                  )
                : IconButton(
                    icon: Icon(
                      Icons.volume_off,
                      color: Theme.of(context).primaryColor,

                      // color: Colors.white,
                    ),
                    onPressed: () async {
                      if (true) {
                        widget.audioPlayer.play(
                          "http://stream.zeno.fm/hxm8m339xa0uv",
                        );
                        setState(() {
                          isPlaying = true;
                        });
                      }
                    });
          }
        } else {
          return Text("L");
        }
      },
    );
  }
}
