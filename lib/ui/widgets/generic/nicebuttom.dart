import 'package:flutter/material.dart';

class NiceButton extends StatelessWidget {
  NiceButton({
    this.onPressed,
    this.child,
    this.bgColor = Colors.white,
    this.textColor = Colors.black,
    this.radius = 30.0,
  });
  final onPressed;
  final child;
  final bgColor;
  final textColor;
  final double radius;
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () => this.onPressed(),
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(radius),
      ),
      color: this.bgColor,
      elevation: 5,
      textColor: this.textColor,
      child: this.child,
    );
  }
}
