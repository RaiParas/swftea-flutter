import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:swfteaproject/providers/SwfTeaController.dart';
import 'package:swfteaproject/ui/widgets/generic/dialougeBox.dart';

class ViewImage extends StatefulWidget {
  final Controller controller = Get.find();
  final dynamic arguments = Get.arguments;
  @override
  _ViewImageState createState() => _ViewImageState();
}

class _ViewImageState extends State<ViewImage> {
  int currentIndex = 0;
  int downloadprogress = 0;

  String _message = "";
  String _path = "";
  String _size = "";
  String _mimeType = "";
  File _imageFile;
  int _progress = 0;

  @override
  Widget build(BuildContext context) {
    List<Widget> _actions = [];
    if (currentIndex >= 0) {
      if ((widget.arguments['type'] ?? 'network') == 'network') {
        _actions.add(IconButton(
          icon: Icon(Icons.cloud_download),
          onPressed: () async {
            Get.dialog(
              CustomDialog(
                title: "Success",
                child: Text('Saved to gallery.'),
              ),
            );
            _downloadImage(widget.arguments['images'][currentIndex]['path']);
          },
        ));
      }
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Photo View'),
        actions: _actions,
      ),
      body: PageView.builder(
        itemBuilder: (context, index) {
          return widget.arguments['type'] == 'asset'
              ? getLocalImage(widget.arguments['images'][index])
              : getNetworkImage(widget.arguments['images'][index]['path']);
        },
        itemCount: widget.arguments["images"].length,
        onPageChanged: (value) {
          setState(() {
            currentIndex = value;
          });
        },
      ),
    );
  }

  Future<void> _downloadImage(String url,
      {AndroidDestinationType destination,
      bool whenError = false,
      String outputMimeType}) async {
    String fileName;
    String path;
    int size;
    String mimeType;
    try {
      String imageId;

      if (whenError) {
        imageId = await ImageDownloader.downloadImage(url,
                outputMimeType: outputMimeType)
            .catchError((error) {
          if (error is PlatformException) {
            var path = "";
            if (error.code == "404") {
              print("Not Found Error.");
            } else if (error.code == "unsupported_file") {
              print("UnSupported FIle Error.");
              path = error.details["unsupported_file_path"];
            }
            setState(() {
              _message = error.toString();
              _path = path;
            });
          }

          print(error);
        }).timeout(Duration(seconds: 10), onTimeout: () {
          print("timeout");
          return;
        });
      } else {
        if (destination == null) {
          imageId = await ImageDownloader.downloadImage(
            url,
            outputMimeType: outputMimeType,
          );
        } else {
          imageId = await ImageDownloader.downloadImage(
            url,
            destination: destination,
            outputMimeType: outputMimeType,
          );
        }
      }

      if (imageId == null) {
        return;
      }
      fileName = await ImageDownloader.findName(imageId);
      path = await ImageDownloader.findPath(imageId);
      size = await ImageDownloader.findByteSize(imageId);
      mimeType = await ImageDownloader.findMimeType(imageId);
    } on PlatformException catch (error) {
      setState(() {
        _message = error.message;
      });
      return;
    }

    if (!mounted) return;

    setState(() {
      var location = Platform.isAndroid ? "Directory" : "Photo Library";
      _message = 'Saved as "$fileName" in $location.\n';
      _size = 'size:     $size';
      _mimeType = 'mimeType: $mimeType';
      _path = path;

      if (!_mimeType.contains("video")) {
        _imageFile = File(path);
      }
      return;
    });
  }

  Widget getNetworkImage(url) {
    return InteractiveViewer(
      panEnabled: false, // Set it to false to prevent panning.
      boundaryMargin: EdgeInsets.all(80),
      minScale: 0.5,
      maxScale: 4,
      child: Image.network(url),
    );
  }

  Widget getLocalImage(String url) {
    return InteractiveViewer(
      panEnabled: false, // Set it to false to prevent panning.
      boundaryMargin: EdgeInsets.all(80),
      minScale: 0.5,
      maxScale: 4,
      child: Image.file(
        File(url),
      ),
    );
  }
}
